setx /M PATH "%PATH%;C:\Program Files\Microsoft VS Code\bin" && ^
refreshenv && ^
code --install-extension ms-vscode.vscode-typescript-tslint-plugin && ^
code --install-extension rbbit.typescript-hero && ^
code --install-extension esbenp.prettier-vscode && ^
code --install-extension mrmlnc.vscode-pugbeautify && ^
code --install-extension ditto.convert-html-to-pug && ^
code --install-extension msjsdiag.debugger-for-chrome && ^
code --install-extension ms-azuretools.vscode-docker
