cinst -y --no-progress notepadplusplus keepass keepass-plugin-keeagent keepass-plugin-keeanywhere googlechrome 7zip tortoisegit git mremoteng visualstudiocode dotnetcore-sdk nodejs visualstudio2017enterprise visualstudio2017-workload-webbuildtools visualstudio2017-workload-netweb visualstudio2017-workload-netcoretools visualstudio2017-workload-azure visualstudio2017-workload-nativedesktop nuget.commandline linqpad resharper resharper-platform resharpercpp vagrant python wixtoolset r.project

cinst -y -source webpi ServiceFabricSDK_3_0_CU2

python -m pip install --upgrade pip

pip install ipykernel numpy scipy pandas matplotlib jupyter